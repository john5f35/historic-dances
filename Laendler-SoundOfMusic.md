# Laendler from Sound of Music
Largely faithful to the choreography of the dance in the movie,
with added bits taken from another traditional Austrian Laendler dance.

## External References
- [video](https://youtu.be/Zwb_A12zXEM)
- [video 2](https://youtu.be/qUfWRBGQkz0)
- [added bits](https://youtu.be/ggmmgkiiGac)
- [music](https://open.spotify.com/track/5nxBKbViyXlwwIIoDtUKHL?si=QKON9ofuSeSuRXsmP18qgA)

## Music structure
| bars  |                     section key                     |
| ----- | --------------------------------------------------- |
| 8 * 2 | [intro](#intro)                                     |
| 4     | [honour](#honour)                                   |
| 8     | [walk & spin](#walk-&-spin)                         |
| 8     | [twirl & wrap](#twirl-&-wrap)                       |
| 8     | [leadout & back](#lead-out-&-back)                  |
| 8     | [twirl & promenade](#twirl-&-promenade)             |
| 8     | [balance](#balance)                                 |
| 8     | [waist waltz & twirl out](#waist-waltz-&-twirl-out) |
| 8     | [solos](#solos)                                     |
| 8     | [chase & window wash](#chase-&-window-wash)         |
| 8     | [allemande](#allemande)                             |
| 8     | [mazurka waltz](#mazurka-waltz)                     |
| 4     | [waltz finale](#waltz-finale)                       |



## Dance Description
### Intro
#### movie version
  Formation: 2 circles W inside M outside

  - (8) M promenade ACW, W promenade CW.
  - (6) M with incoming W join Lh over Rh, low promenade ACW
  - (2) M twirl W out to face.
#### 2 person version
  Formation: Start apart facing

  - (8) S shape gypsy on Rsh: 4 bars arc into centre, 4 bars arc over Lsh apart facing.
  - (6) CW arc in, join Lh over Rh, low promenade ACW
  - (2) M twirl W out to face

### Honour
  - (4) Step, bow, join inside hands, W arm rest on M arm, face ACW along LoD.

### walk & spin
  - (4) Starting both Lf free, step-lift for (3); stomp to face, swinging joint hands back open.
  - (4) M swing W forward spinning CW while clapping & stomping (3); Stomp to face, join both hands.

### twirl & wrap
  - (4) Swing forward both hands raising over W turning CW (2); same for M.
  - (4) Releasing front, M swing W back ACW over raised Rh twice around (2); lower joint hands, W continue turning, wrapping herself with joint hands over neck, stomp (1); unwrap finish facing (1)

### leadout & back
  - (4) Lead out, swing joint inside hands forward & back; finish facing joining both hands
  - (4) Mazurka (glide-cut-hop) back (2); both wheel twirl to face (2); **switch to joining Rh high**

### twirl & promenade
  - (4) M twirl W CW (6 times?) (3); catch Lh, continue twirling over, finish lower both hands into low promenade hold, Lh over Rh (Lh over M chest level).
  - (4) promenade ACW (3); twirl W ACW to face (1), finish elbow to elbow, Rh over Lh.

### balance
  - (4) Rf waltz step balance L; Lf waltz step balance R (2); twirl switching place (M ACW around W), finish **Lf free**.
  - (4) counter-part (balance R, L, twirl w. M CW around W) (3); twirl W CW, with both hands high **switching to open hands**

### waist waltz & twirl out
  - (4) waltz while slowly dropping both hands on W waist (2); waltz (1); (rel. Lh) M twirl W CW w. Rh
  - (4) (coming out of twirl) swing joint hands forward pointing inside foot (1); step, swinging hands back, pointing outside foot (1); M twirl W CW out releasing hands, W keep turning, finish facing away from M (2).

### solos
  <!-- TODO: orientation with couples in a circle? -->
  - (4) W Lf mazurka (glide-cut-hop) ACW around M (3); finish turning to face M (1); all while M clapping (p-c-c)
  - (4) M clapping while waltz CW to W back (2); both turn single CW (2) finish M back to W

### chase & window wash
  - (4) both waltz forward, W give Rh over M Lsh (2); joinning Rh, M pull W in front twirling ACW (2); finish elbow to elbow joining Lh over Rh.
  - (4) W back M window washing (2); (with Lh on top) twirl W ACW once (1); twirl W CW, twist Lh behind W back, finish in high allemande hold.

### allemande
  - (4) allemande ACW (2); twirl W to switch Lh on high (2)
  - (4) allemande CW (2); twirl W out to face, finishing ballroom hold.
### mazurka waltz
  - (4) mazurka forward (glide-cut-hop), redowa (turning as couple) (3);
  - (4) same over shouulder
### waltz finale
  - (4) waltz on, turning W out, bow to finish.