# Dance Compositions

## 12 Contredanses, WoO 14: No.9 in A Major

### General Info:
* Formation: longway sets as many as will
* Music: AABB
* [Link to music](https://open.spotify.com/track/4rethMt5Pmu6UIxJICa4yW?si=VZwpuxJLSJWpGHsjnjcHYw)

### Instructions:
* {A} [8] 1s cross cast below 2s / half figure 8 up
* {A} [8] 2s do the same
* {B} [8] the figure
    * [4] all 4 face ACW around square with right foot free, then in the order of 1M 1W 2W 2M, each _jeté_ _assemblé_ forward short of the length of one side of the square slightly to the right, finishing facing out.
    * [4] all _chassé_ left and turn single over right shoulder, finishing improper with 1s on top.
* {B} [8] repeat the same figure with 1s in progressed position.

## 12 Contredanses, WoO 14: No.12 in E-Flat Major

### General Info:
* Formation:
* Music: A{8}AB{8}BC{8}CD{16}A{8}B'{8}

### Instructions:


## 12 German Dances, WoO 8: No.1 in C Major

### General Info:
* Formation:
* Music: AABB

### Instructions:
