# Easy English Country Dances
[Spotify playlist](https://open.spotify.com/user/12130070303/playlist/5UxNSgrRqSfAlculpMD5Op?si=HAI4SFxrTAqai_FjdgEWog)

## Comical Fellow
### General Info
|                   |                           |
| ----------------- | ------------------------- |
| Formation         | Longways Triple Minor     |
| Music Structure   | 6/8 A{8}AB{12}B           |
| Start Foot        | right foot                |

### Instructions
| Section   | Instructions                                                      |
| :-------: | ----------------------------------------------------------------- |
| A1        | 1M2W set f.; retire / 2h t.                                       |
| A2        | 1W2M same                                                         |
| B1        | 1s lead down / back; cast / clapx4; t.s.                          |
| B2        | Circle 6 l&r // clapx4; 2h t. (or t.s.)                           |

### Notes
- BBC Pride and Prejudice had it doing duple minor,
    and cross by r.sh after clapping.
- there might not be enough time to do two-hand turn at the end,
    in which case maybe just do a turn single.

## Jameko / Jamaica / La Bonne Amitie
|                   |                           |
| ----------------- | ------------------------- |
| Formation         | Longways Duple Minor      |
| Music Structure   | 2/2 A{4}AB{8}B            |
| Start Foot        | right foot                |

### Instructions
| Section   | Instructions                                                      |
| :-------: | ----------------------------------------------------------------- |
| AA        | 1s shake r., l.; on crossed hs t. 1/2 / same w. neigh.            |
| B1        | f. & b., *1/2 fig. up*                                            |
| B2        | 2h t. neigh. / 2h t. partner                                      |

### Notes
- B2 was added due to repetition in music

## The Dashing White Sergeant
* Formation: trios facing around circle
* Music: AABB
#### The Dance
* {A}[8]: all circle left & right
* {A}[8]: centre sets to right, right arm turn; same on left
* {B}[8]: *hay* on own lines, starting with right person (right shoulder).
* {B}[8]: lines forward & back, forward & pass right shoulder

## The Lancer's Quadrille (The Hart Lancers)
|                   |                           |
| ----------------- | ------------------------- |
| Formation         | Longways Duple Minor      |
###